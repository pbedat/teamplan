package main

import (
	"time"
)

var db *database = &database{}

type database struct {
	invites []inviteDto
	users   []user
}

func (db *database) insertInvite(invite inviteDto) {
	db.invites = append(db.invites, invite)
}

func (db *database) findInviteByToken(token string) *inviteDto {
	for _, inv := range db.invites {
		if inv.token == token {
			return &inv
		}
	}
	return nil
}

type inviteDto struct {
	id         string
	token      string
	name       string
	email      string
	createdAt  time.Time
	acceptedAt *time.Time
}

type user struct {
	id        string
	name      string
	inviteId  string
	createdAt time.Time
}
