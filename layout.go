package main

import (
	"context"

	. "github.com/theplant/htmlgo"
)

type layoutProps struct {
	children HTMLComponents
	script   string
}

func layout(children ...HTMLComponent) layoutProps {
	return layoutProps{children: children}
}

func (props layoutProps) MarshalHTML(ctx context.Context) ([]byte, error) {
	return HTML(
		Head(
			Meta().Charset("utf-8"),
			Title("Teamplan"),
			// alpine.js
			RawHTML(`<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>`),
			// bootstrap
			RawHTML(`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
			`),
			RawHTML(`
			<script type="module">
			import { Application, Controller } from "https://unpkg.com/@hotwired/stimulus/dist/stimulus.js"
			window.Stimulus = Application.start()
			</script>`),
			Script(props.script).Type("module"),
		),
		Body().Style("padding-top: 6rem;").Children(
			Nav().Class("navbar navbar-expand-lg fixed-top bg-light").Children(
				Div().Class("container-fluid").Children(
					A().Class("navbar-brand").Href("/").Text("Teamplan"),
				),
			),
			Div().Class("container").Children(
				props.children...,
			),
		),
	).MarshalHTML(ctx)
}
