# Einladugen

Beginnen wir nun mit der Implementierung des ersten Use Cases: Der Einladung des Personals.
Derjenige, der das Personal einplant, soll zunächst Personen in die Applikation einladen können.
Die Eingeladene Person erhält eine E-Mail mit einem Magic-Link und ist sofort in der Applikation eingeloggt. Die Login Funktion wird aber erst später implementiert.

Damit die Seite nicht ganz so schrecklich aussieht, ist jetzt das Bootstrap CSS eingebunden.
Für einfache interaktive Features nutzen wir alpine.js.
Beides findet sich in der [layout.go](../layout.go)

Wir beginnen daher in der Datei [invites_index.go](../invites_index.go)
