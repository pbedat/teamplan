# Templates

Selbstverständlich enthält das Go Standardframework bereits eine gutes und sehr performantes Templating-Paket `html/template`. Wer allerdings erst mal auf den JSX/TSX Geschmack gekommen ist, möchte stark typisierte Templates und eine Komponentenarchitektur nicht mehr gerne missen. Deshalb nutzen wir in diesem Projekt das Modul `htmlgo`.

```bash
go get github.com/theplant/htmlgo
```

Weiter geht's in der [layout.go](../layout.go)
