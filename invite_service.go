package main

import (
	"fmt"
	"os"
	"time"

	"github.com/google/uuid"
)

type _inviteService struct{}

var inviteService _inviteService

func (_inviteService) createInvite(name string, email string) error {
	invite := inviteDto{
		id:        uuid.NewString(),
		token:     uuid.NewString(),
		name:      name,
		email:     email,
		createdAt: time.Now(),
	}

	db.insertInvite(invite)

	body := fmt.Sprintf(
		`Mit diesem <a href="http://localhost:8080/invites/redeem?token=%s">Link</a>
		kannst du dich bei Teamplan einloggen und deine Verfügbarkeit eintragen.`, invite.token)

	return sendMail(mailDto{
		from:    "webmaster@teamplan.io",
		subject: "Deine Einladung zu Teamplan",
		body:    body,
		to:      email,
	})
}

type mailDto struct {
	to      string
	from    string
	subject string
	body    string
}

func sendMail(mail mailDto) error {

	inboxFolder := fmt.Sprintf("./.mails/%s", mail.to)

	if err := os.MkdirAll(inboxFolder, 0777); err != nil {
		return err
	}

	mailFile := fmt.Sprintf("%s/%d_%s.html", inboxFolder, time.Now().UnixMilli(), mail.subject)

	f, err := os.Create(mailFile)

	if err != nil {
		return fmt.Errorf("could not create mail file: %w", err)
	}

	defer f.Close()

	_, err = f.WriteString(mail.subject + "\n-----\n" + mail.body)

	if err != nil {
		return fmt.Errorf("could not write to mail file: %w", err)
	}

	return nil
}
