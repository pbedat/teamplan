package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/theplant/htmlgo"
	. "github.com/theplant/htmlgo"
)

// hier gibt es nicht viel neues
// die createInvitesPage Seite wird gerendert
func renderCreateInvitesPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "text/html")
	w.WriteHeader(200)

	htmlgo.Fprint(w, layout(createInvitesPage()), r.Context())
}

// Wird die <form> auf der createInvitesPage abgeschickt, kümmert sich der folgnede
// Handler um den POST request (Mapping siehe, wie immer, main.go)
func postInvite(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Content-Type", "text/html")

	// zunächst müssen wir die gesendeten Formulardaten parsen
	// da es hier zu einem Fehler kommen kann, wenn z.B. die Daten nicht ordentlich
	// enkodiert wurden, liefert `ParseForm` einen `error` zurück.
	// Das ist die Alternative zu try/catch Blöcken in Go. Richtige Exceptions werden
	// in Go `panic` genannt sollten wirklich nur eingesetzt werden, wenn ein Fehler nicht
	// sinnvoll behandelt werden kann.
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	names := r.PostForm["name"]
	emails := r.PostForm["email"]

	valid, nonUniqueEmail := validateUnique(emails)

	if !valid {
		// StatusUnprocessableEntity nutzen wir, da das Turbo Framework (später)
		// so mit Validierungen besser umgehen kann.
		w.WriteHeader(http.StatusUnprocessableEntity)
		htmlgo.Fprint(w,
			layout(
				createInvitesPage().
					// wir rendern die Übergebenen daten...
					persons(names, emails).
					// und zusätzlich eine Fehlermeldung
					error(
						fmt.Sprintf("Die E-Mail '%s' ist nicht eindeutig", nonUniqueEmail))),
			r.Context())
		return
	}

	for i, name := range names {
		// Beim Erstellen einer Einladung wird diese in einer In-Memory Datenbank gespeichert
		// die Einladungen werden nicht wirklich per E-Mail verschickt, sondern im Ordner .mails
		// im Dateisystem abgelegt
		if err := inviteService.createInvite(name, emails[i]); err != nil {
			// bei einem unerwarteten Fehler geht es nicht weiter
			log.Printf("create invite failed: %e", err)
			w.WriteHeader(500)
			return
		}
	}

	// ein erfolgreicher submit führt zurück zur invites Seite
	w.Header().Add("Location", "/invites")
	w.WriteHeader(http.StatusSeeOther)
}

func validateUnique(slice []string) (bool, string) {
	m := make(map[string]bool)

	for _, s := range slice {
		exists := m[s]
		if exists {
			return false, s
		}
		m[s] = true
	}

	return true, ""
}

//#region createInvitesPage

type createInvitesPageProps struct {
	names           []string
	emails          []string
	validationError string
}

func createInvitesPage() createInvitesPageProps {
	return createInvitesPageProps{
		names:  []string{""},
		emails: []string{""},
	}
}

func (props createInvitesPageProps) persons(names []string, emails []string) createInvitesPageProps {
	props.names = names
	props.emails = emails
	return props
}

func (props createInvitesPageProps) error(err string) createInvitesPageProps {
	props.validationError = err
	return props
}

func (props createInvitesPageProps) props(setProps func(props createInvitesPageProps)) createInvitesPageProps {
	setProps(props)
	return props
}
func (props createInvitesPageProps) MarshalHTML(ctx context.Context) ([]byte, error) {
	return Section(
		H1("Personen Einladen"),
		Form().Method(http.MethodPost).
			// Damit man der Liste der einzuladenden Personen interaktiv weitere
			// Personen hinzufügen kann, nutzen wir alpine.js.
			// alle mit x-* gekennzeichneten Attribute gelten für dieses Framwork.
			// zu allererst wird mit x-data die Anzahl der gerenderten Personen als state
			// festgehalten, um dynamisch den "remove" Button anzuzeigen, wenn es mehr als
			// eine Person gibt
			Attr("x-data", fmt.Sprintf("{count: %d}", len(props.names))).Children(
			Div().Class("card").Children(
				Div().Class("list-group list-group-flush").
					// Die Referenz auf diese Liste is notwendig, um aus dem Template unten,
					// neue Listeneinträge in dieses HTML Element hinzuzufügen
					Attr("x-ref", "list").
					Children(
						ComponentFunc(func(ctx context.Context) (r []byte, err error) {
							children := make(HTMLComponents, len(props.names))

							for i, name := range props.names {
								children[i] = inviteFieldset().name(name).email(props.emails[i])
							}

							return Components(children).MarshalHTML(ctx)
						}),
						Template(inviteFieldset()).Attr("x-ref", "fieldset"),
					),
				Div().Class("card-body").Children(
					Div(Button("Weitere Person einladen").Type("button").
						Class("btn btn-light").
						// mit einem Click auf diesen Button wird einerseits der count erhöht
						// und andererseits ein neuer Listeneintrag der Liste der Personen hinzugefügt
						Attr(
							"@click",
							"count = count + 1; $refs.list.append($refs.fieldset.content.cloneNode(true))")),
				),
			),
			Div().Class("my-4 d-flex align-items-baseline").Children(
				Button("Einladung versenden").Class("btn btn-primary me-2"),
				Text(" oder "),
				A().Href("/invites").Class("ms-2").Text("zurück"),
			),
			If(props.validationError != "", Div().Class("alert alert-danger").Text(
				props.validationError,
			)),
		),
	).MarshalHTML(ctx)

}

//#endregion

type inviteFieldsetProps struct {
	_name      string
	_email     string
	_canRemove bool
}

func inviteFieldset() inviteFieldsetProps {
	return inviteFieldsetProps{}
}

func (props inviteFieldsetProps) name(name string) inviteFieldsetProps {
	props._name = name
	return props
}

func (props inviteFieldsetProps) email(email string) inviteFieldsetProps {
	props._email = email
	return props
}

func (props inviteFieldsetProps) MarshalHTML(ctx context.Context) ([]byte, error) {
	return Div().Class("list-group-item d-grid").Attr("x-data", "{item: $el}").Children(
		Template().Attr("x-if", "count > 1").Children(
			Button("remove").Type("button").Class("btn btn-link btn-sm ms-auto").
				Attr("@click", "count = count - 1; item.remove()"),
		),
		Div().Class("d-flex align-items-baseline mb-2").Children(
			Label("Name*:").Class("form-label me-2"),
			Input("name").Class("form-control").Value(props._name).Required(true),
		),
		Div().Class("d-flex align-items-baseline mb-2").Children(
			Label("Email*:").Class("form-label me-2"),
			Input("email").Class("form-control").Value(props._email).Type("email").Required(true),
		),
	).MarshalHTML(ctx)
}

// Weiter gehts in der invites_create_test.go
