https://go.dev/doc/install

# teamplan

Dieses Tutorial-Projekt soll anhand einer einfachen Web-Applikation einen Einstieg in die Web-Entwicklung mit Go für JavaScript-Entwickler bieten.
Das Repository wird Commit für Commit verschiedene Aspekte und Herangehensweisen beleuchten und so Schritt für Schritt
eine Web-Anwendung zur Personalplanung implementieren.

Mit dem ersten Commit werden wir einen Web-Server starten, der das typische Hello-World über HTTP ausgeben wird.

Wechselt nun in den ersten [Commit](https://gitlab.com/pbedat/teamplan/-/commit/21f3b22465de1bce2a8b07e9965a382c2299b56e):

```bash
SHA1=$(git rev-list main | tail -n $1 | head -n 1)
git checkout $SHA1
```

Die Anwendung ist in jedem Commit mit `go run .` ausführbar und startet unter localhost:8080
