package main

import (
	"net/http"

	. "github.com/theplant/htmlgo"
)

// Dieser Handler sorgt später einmal dafür, dass sich eingeladene Personen direkt
// einloggen können.
// Aktuell wird nur geprüft ob die Einladung überhaupt angenommen werden kann
// Weiter gehts im nächsten Commit
func renderInviteRedeemPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	token := r.URL.Query().Get("token")

	invite := db.findInviteByToken(token)

	w.WriteHeader(200)

	if invite == nil {
		Fprint(
			w,
			layout(
				Div().Class("alert alert-info").
					Text("Diese Einladung existiert nicht"),
			),
			r.Context())
		return
	}

	if invite.acceptedAt != nil {
		Fprint(
			w,
			layout(
				Div().Class("alert alert-info").
					Text("Diese Einladung wurde bereits angenommen"),
			),
			r.Context())
		return
	}

	Fprint(
		w,
		layout(
			Text("Login noch nicht implementiert..."),
		),
		r.Context())
}
