package main

import (
	"net/http"

	"github.com/theplant/htmlgo"
	. "github.com/theplant/htmlgo"
)

func main() {

	http.HandleFunc("/invites", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			renderInvitesPage(w, r)
			break
		}
	})
	http.HandleFunc("/invites/create", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			renderCreateInvitesPage(w, r)
		case http.MethodPost:
			postInvite(w, r)
		}
	})
	http.HandleFunc("/invites/redeem", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			renderInviteRedeemPage(w, r)
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(200)

		htmlgo.Fprint(w,
			layout(
				P(Text("Hello World")),
			),
			r.Context(),
		)
	})

	http.ListenAndServe(":8080", nil)
}
