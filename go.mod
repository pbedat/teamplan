module gitlab.com/pbedat/teamplan

go 1.18

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/google/uuid v1.3.0
	github.com/theplant/htmlgo v1.0.3
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
)
