package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/PuerkitoBio/goquery"
)

// *_test.go Dateien enthalten per Konvention, die sich z.B. über `go test .` ausführen lassen
// Die Tests prüfen ob die Validierung im POST Request korrekt durchgeführt wird.
// Weiter geht's in invites_redeem.go
func TestValidation(t *testing.T) {

	t.Run("equal names should be fine", func(t *testing.T) {
		body := make(url.Values)

		body.Add("name", "foo")
		body.Add("email", "q@foo.de")

		body.Add("name", "foo")
		body.Add("email", "q@qux.de")

		req, err := http.NewRequest("POST", "/invites/create", bytes.NewBufferString(body.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		if err != nil {
			t.Errorf("An error occurred. %v", err)
		}

		rr := httptest.NewRecorder()

		http.HandlerFunc(postInvite).ServeHTTP(rr, req)

		if rr.Code != http.StatusSeeOther {
			t.Errorf("expected status code 303, but got %d", rr.Code)
		}
	})

	t.Run("non unique urls should fail", func(t *testing.T) {
		body := make(url.Values)

		body.Add("name", "foo")
		body.Add("email", "q@q.de")

		body.Add("name", "bar")
		body.Add("email", "q@q.de")

		req, err := http.NewRequest("POST", "/invites/create", bytes.NewBufferString(body.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		if err != nil {
			t.Errorf("An error occurred. %v", err)
		}

		rr := httptest.NewRecorder()

		http.HandlerFunc(postInvite).ServeHTTP(rr, req)

		if rr.Code != http.StatusUnprocessableEntity {
			t.Errorf("expected status code 422, but got %d", rr.Code)
		}

		doc, err := goquery.NewDocumentFromReader(rr.Result().Body)

		if err != nil {
			t.Error(err)
		}

		if doc.Find(".alert.alert-danger").Length() <= 0 {
			t.Errorf("expected validation alert")
		}
	})
}
