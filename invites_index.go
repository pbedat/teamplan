package main

import (
	"context"
	"net/http"

	"github.com/theplant/htmlgo"
	. "github.com/theplant/htmlgo"
)

// Das hier ist eine Funktionsignatur bereits kennen. Diese Funktion ist ein
// http handler und wird in main.go der Route "/invites" zugeordnet
func renderInvitesPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "text/html")
	w.WriteHeader(200)
	htmlgo.Fprint(w, layout(invitePage(db.invites)), r.Context())
}

// Für komplexere Komponenten mit htmlgo bedienen wir uns einer Analogie aus React
// zunächst definieren wir die Properties, die eine Kompnente erhalten kann
type invitePageProps struct {
	invites []inviteDto
}

// dann eine Initialisierungsfunktion für die Props
func invitePage(invites []inviteDto) invitePageProps {
	return invitePageProps{invites}
}

// und zu guter letzt eine Render-Funktion
// Die Vorschrift func (<target>) <name>(...) { }
// bedeutet, dass wir die Funktion auf die Instanzen des Targets binden, damit
// so ewtwas möglich ist:
// ```go
// props := invitePageProps{}
// props.MarshalHTML(ctx)
// ```
// Durch die Definition der `MarshalHTML` Funktion implementieren wir das htmlgo
// Interface `HTMLComponent` für die `invitePageProps`. Eine besondere Kennzeichnung,
// z.B. durch ein `implements` oder `extends` Keyword, is in Go nicht notwendig.
//
// die invitePage rendert die erstellten Einladungen in einer Tabelle:
func (props invitePageProps) MarshalHTML(ctx context.Context) ([]byte, error) {
	return Components(
		Header(
			H1("Einladungen"),
			P(A().Class("btn btn-primary").Href("/invites/create").Text("Neuen Benutzer einladen")),
		),
		Main(
			Table().Class("table").Children(
				Thead(
					Tr(
						Th("Name"),
						Th("Eingeladen am"),
						Th("Angenommen am"),
					),
				),
				Tbody(
					// Mit der `ComponentFunc` können wir das Rendering dynamisch gestalten
					ComponentFunc(func(ctx context.Context) (r []byte, err error) {

						if len(props.invites) == 0 {
							return Tr(
								Td().Attr("colspan", 3).Children(
									Text("Es wurden noch keine Benutzer "),
									A().Href("/invites/create").Text("eingeladen"))).MarshalHTML(ctx)
						}

						children := make(HTMLComponents, len(props.invites))

						for i, invite := range props.invites {
							children[i] = Tr(
								Td(Text(invite.name)),
								// Der Format String wirkt auf den ersten Blick komisch, aber
								// jeder formatierbare Parameter hat einen Stellenwert:
								// 1   2  3  4  5  6    -7
								// Jan 2. 15:04:05 2006 MST
								Td(Text(invite.createdAt.Format("02.01.2006"))),
								Td(
									// mit If kann man konditional rendern
									If(invite.acceptedAt == nil,
										Text("-")),
									// Iff ist hilfreich, wenn der Wert ohne die Bedingung nicht renderbar ist
									// hier wäre z.B. acceptedAt nil und es würde zu einem panic kommen
									Iff(invite.acceptedAt != nil,
										func() HTMLComponent { return Text(invite.acceptedAt.Format("02.01.2006")) }),
								),
							)
						}
						return children.MarshalHTML(ctx)
					}),
				),
			),
		),
	).MarshalHTML(ctx)
}

// weiter gehts in der invites_create.go
